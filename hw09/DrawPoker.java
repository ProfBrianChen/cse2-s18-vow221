//Vasilios Oliver Walsh
import java.util.Arrays;
import java.util.Random;

public class DrawPoker {
  public static void main(String[] args) {
    
  int[] cards = new int[52]; // Creates a new array of length 52 called 'cards' 
    for (int i = 0; i < 52; i++) { //making sure all members of array are j 
      cards[i] = i; //setting each value 
    }
    Shuffle(cards); //shuffle deck 

  int[] playerOne = new int[5]; //creates an array for each player's hand 
  int[] playerTwo = new int[5]; 
    for (int x = 0; x < 5; x++) {
      playerOne[x] = cards[2 * x];
      playerTwo[x] = cards[2 * x + 1];
    }
    
    System.out.println("Player One's Hand: "); //prints out player's hands
    for (int x : playerOne) {
    if(cards[x] % 13 < 1){
      String name = " of hearts";
      System.out.println(x + " " + name);
    }
    
    else if(cards[x] % 13> 1 && cards[x] % 1 < 2){
      String name = " of diamonds";
      System.out.println(x + " " + name);
    }
    
    else if(cards[x] % 13 > 2 && cards[x] % 13 < 3){
      String name = " of spades";
      System.out.println(x + " " + name);
    }
    
    else if(cards[x] % 13 > 3 && cards[x] % 13 < 4){
      String name = " of clubs";
      System.out.println(x + " " + name);
    }
      
    }

    System.out.println("Player Two's Hand: ");
    for (int x : playerTwo) {
    if(cards[x] % 13 < 1){
      String name = " of hearts";
      System.out.println(x + " " + name);
    }
    
    else if(cards[x] % 13> 1 && cards[x] % 1 < 2){
      String name = " of diamonds";
      System.out.println(x + " " + name);
    }
    
    else if(cards[x] % 13 > 2 && cards[x] % 13 < 3){
      String name = " of spades";
      System.out.println(x + " " + name);
    }
    
    else if(cards[x] % 13 > 3 && cards[x] % 13 < 4){
      String name = " of clubs";
      System.out.println(x + " " + name);
    }
      
    }
  int playerOneScore = 0; //initalizes player scores 
  int playerTwoScore = 0;   
    if (Pair(playerOne)) { // Pairs method
      playerOneScore += 1;
    }
      else if (Pair(playerTwo)) {
      playerTwoScore += 1;
    }
    
    if (ThreeOfAKind(playerOne)) { // Three of a Kind method
      playerOneScore += 2;
     }
      else if (ThreeOfAKind(playerTwo)) {
      playerTwoScore += 2;
     }
    
    if (Flush(playerOne)) { // Flush method
      playerOneScore += 4;
    }
    else if(Flush(playerTwo)) {
      playerTwoScore += 4;
    }
   
    if (FullHouse(playerOne)) {  // Full House method
      playerOneScore += 8;
    }
    else if (FullHouse(playerTwo)) {
      playerTwoScore += 8;
    }
    
 if (playerOneScore == playerTwoScore) {
    if (CardCompare(playerOne, playerTwo)) {
        playerOneScore += 1;
    }
     else {
        playerTwoScore += 1;
     }
   }

    if (playerOneScore > playerTwoScore) { // if statement which determines the output statement for the winner
      System.out.println("Player One Wins");
    }
    else {
      System.out.println("Player Two Wins");
    }
    

  }
  

  public static void Shuffle(int[] y) { // shuffle method
    for (int i = 0; i < y.length; i++) {
      int rand = (int) (Math.random() * y.length);
      int temp = y[i];
      y[i] = y[rand];
      y[rand] = temp;
    }
  }
    
  public static boolean Pair(int[] hand) { // pair method
    for (int i = 1; i < hand.length; i++) { // initializes i for loop 
      if (hand[i] % 13 == hand[0] % 13) {  
        return true;
      }
    }
    return false;
  }
    
  public static boolean ThreeOfAKind(int[] hand) { // three of a kind method
    for (int i = 1; i < hand.length; i++) { 
      for (int k = i + 1; k < hand.length; k++) { 
        if (hand[0] % 13 == hand[i] % 13 && hand[0] % 13 == hand[k] % 13 ) {
          return true;
        }
      }
    } 
    return false;
  }

  public static boolean Flush(int[] hand) { // flush method
    int count = 0;
    for (int i = 1; i < hand.length; i++) {
      if (hand[0] / 13 == hand[i] / 13) {
        count += 1;
      }
    }
    if (count == 4) {
      return true;
    }
    return false;
  }

public static boolean FullHouse(int[] hand) { // full house method
 if (Pair(hand) && ThreeOfAKind(hand)) {
   return true;
   }
   return false;
 }

public static boolean CardCompare(int[] handOne, int[] handTwo) { 
 int topHandOne = handOne[0];
  for (int i = 0; i < handOne.length; i++) {
    if (handOne[i] > topHandOne) {
        topHandOne = handOne[i];
      }
    }
 int topHandTwo = handTwo[0];
  for (int i = 0; i < handTwo.length; i++) {
    if (handTwo[i] > topHandTwo) {
        topHandTwo = handTwo[i];
      }
    }
  if (topHandTwo > topHandTwo) {
    return true;
    }
    return false;
 }

}
