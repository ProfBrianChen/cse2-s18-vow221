//Vasilios Oliver Walsh, February 2nd, 2018, CSE 2
//lab02, Cyclometer describing the number of minutes of each trip, number of counts, distances in miles, and distance for both trips combined. 
//
public class Cyclometer {
    //main method required for every Java program
    public static void main(String[] args) {
    int secsTrip1=480; // This is the first trip's length in seconds
    int secsTrip2=3220; // This is the second trip's length in seconds
    int countsTrip1=1561; // This is the number of times the wheel turned during the first trip
    int countsTrip2=9037; // This is the number of times the wheel turned during the second trip
    double wheelDiameter=27.0, //This double represents the diameter of the bike's wheel
    PI=3.14159,// This double represents the value of PI
    feetPerMile=5280, // This integer defines the length of a mile in feet
	  inchesPerFoot=12,   // This integer defines the length of a foot in inches
    secondsPerMinute=60; // This integer defines the length of a minute in seconds
    double distanceTrip1, distanceTrip2,totalDistance;  // This double defines the length of both trips one and two, as well as the total distance of both combined trips
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
      //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");

	

    } //end of main method
} //end of class  