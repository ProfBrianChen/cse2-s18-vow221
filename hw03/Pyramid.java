import java.util.Scanner;
public class Pyramid {
  public static void main (String [] args) {
    //main method required for every java program
    Scanner input = new Scanner(System.in); // Creates a new scanner
    System.out.print("The base of the pyramid is (input length): "); // prints the prompt for the user to input the length
    double length = input.nextDouble(); // states that the next double is the inputted length
    System.out.print("The height of the pyramid is (input height): "); // prints the prompt for the user to input the height
    double height = input.nextDouble(); // states that the next double is the inputted height
    double volume = (length * length * height)/3; // tells Java to multiply the different variables to find the volume
    System.out.println("The volume of the pyramid is: " + volume); // tells java to print the multiplied variables as volume
    
  } // end of main method
} // end of class
