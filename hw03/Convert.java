import java.util.Scanner;
public class Convert {
  public static void main(String[] args) {
  //main method required for every java program
  Scanner input = new Scanner( System.in );
  System.out.print("Enter the affected area in acres: "); //prompts the user to enter the affected area from the Hurricane
  double areaAcre = input.nextDouble();
  System.out.print("Enter the rainfall within the affected area: "); //prompts the user to enter the total rainfall in the affected areas
  double rainInch = input.nextDouble();
  double sqMilesPerAcre = 0.001562489; // conversion factor to square miles from acres
  double InchPerMile = 63360; // conversion factor from inches to miles 
  double areaMile = areaAcre * sqMilesPerAcre, // amount of miles converted from acres
  rainMile = rainInch / InchPerMile, // amount of rain in miles 
  sqMilesRain = areaMile * rainMile; // calculates amount of rain in square miles 
  System.out.println(sqMilesRain + " cubic miles");
    } //end of main method
  } //end of class