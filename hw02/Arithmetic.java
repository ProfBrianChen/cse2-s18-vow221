public class Arithmetic {
  public static void main(String[] args) { 
int numPants = 3; //Total number of pants
double pantsPrice = 34.98; //Price of pants defined as a variable
int numShirts = 2; //Total number of shirts
double shirtPrice = 24.99; //Costs of shirts
int numBelts = 1; //sets value for number of belts
double beltCost = 33.99; //Cost of belts
double paSalesTax = 0.06; //Pennsylvania tax rate
double totalCostOfPants = (numPants*pantsPrice);   //total cost of pants, including sales tax
double totalCostOfShirts = (numShirts*shirtPrice);   //total cost of shirts, including sales tax
double totalCostOfBelts = (numBelts*beltCost);   //total cost of belts, including sales tax
double salesTaxPants = (numPants*pantsPrice)*paSalesTax; //sales tax on pants
double salesTaxShirts = (numShirts*shirtPrice)*paSalesTax; //sales tax on shirts
double salesTaxBelts = (numBelts*beltCost)*paSalesTax; //sales tax on belts
double costBeforeTax = ((numPants*pantsPrice)+(numShirts*shirtPrice)+(numBelts*beltCost)); //total cost before tax
double totalSalesTax = salesTaxPants+salesTaxShirts+salesTaxBelts; //sales tax total
double totalCost = totalCostOfPants+salesTaxPants+totalCostOfBelts+salesTaxBelts+totalCostOfShirts+salesTaxShirts; //total price including sales tax
    System.out.println("The total price of pants including sales tax is " + totalCostOfPants ); //prints the price of pants
    System.out.println("The total price of shirts including sales tax is " + totalCostOfShirts ); //prints the price of shirts
    System.out.println("The total price of belts including sales tax is " + totalCostOfBelts ); //prints the price of belts
    System.out.println("The sales tax charged when buying pants is " + salesTaxPants ); //prints the sales tax charged on pants
    System.out.println("The sales tax charged when buying shirts is " + salesTaxShirts ); //prints the sales tax charged on shirts
    System.out.println("The sales tax charged when buying belts is " + salesTaxBelts ); //prints the sales tax charged on belts
    System.out.println("The total cost of all purchases before sales tax is applied is " + costBeforeTax ); //prints the total cost of purchases before tax
    System.out.println("The total sales tax added to the purchase is " + totalSalesTax); //total sales tax applied to purchases
    System.out.println("The total cost of all the items purchased, including sales tax, is " + totalCost); //total cost of purchase including sales tax
  }
  
  
  
  
}