//Vasilios Oliver Walsh

public class lab09{
    public static int[] copy(int[] array){
    
    int[] firstArray;
    firstArray = new int [array.length];
    
    for(int i = 0; i < array.length; i++){
      firstArray[i] = array[i];
    }
    
    return firstArray;
    }
    
    public static void inverter(int[] array){
     int temp = 0;
      for(int j = 0; j < array.length/2; j++){
        temp = array[array.length - j - 1];
        array[array.length - j - 1] = array[j];
        array[j] = temp;
      }
    }
     
  public static int[] flip(int[] array){
       int[] flipArray = copy(array);
       inverter(flipArray);
       
       return flipArray; 
     }
  
  public static void print(int[] array){
    System.out.print("{");
      for(int l = 0; l < array.length; l++){
        System.out.print(array[l] + ",");
      }
    System.out.print("}");
  }
  
public static void main(String[] args){
  int[] array0 = { 1, 2, 3, 5, 8, 13, 21, 34 };
  int[] array1 = copy(array0);
  int[] array2 = copy(array0);
  
  inverter(array0);
  print(array0);
  System.out.println(" ");
  
  flip(array1);
  print(array1);
  System.out.println(" ");
  
  int[] array3 = flip(array2);
  print(array3);
  System.out.println(" ");
  }
}