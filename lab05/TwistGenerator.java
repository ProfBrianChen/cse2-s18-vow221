//Vasilios Oliver Walsh
import java.util.Scanner;
public class TwistGenerator {
  public static void main(String[] args) { // main method required for every Java program
    Scanner myScanner = new Scanner (System.in); // scanner input
    int value = 0; // initalizes "value" as 0 
    while (value==0){ // automatically prompts user for input length to define the twist
    System.out.print("Enter a positive integer for length: "); // prompts the user to enter an integer to determine the length of the twist
    if(myScanner.hasNextInt()){
      int length = myScanner.nextInt();
      if (length>0){ // creates an if statement for the correct input
        value++;
        int cross = (length - 1)/3; // defines the cross variable as decremented length divded by 3
        int slash = length; // sets the slashes equal to user input length
        int number1 = 0;
        int number2 = 0;
        int number3 = 0; // sets all three number integers to 0 as a way to initialize the length of the twist
      while(number1<slash){
          System.out.print("\\ "); // outputs the back-slashes on top to form the twist
          number1=number1+2;
          if(number1<slash){
            System.out.print("/"); // other half of twist
            number1++; // increments number 1 if less than slash in order to avoid a compiler error
          }
      }
      System.out.println("");
         while(number2<cross){
           System.out.print(" X "); // outputs the middle of the twist in accordance with the length
           number2++; 
         }
        System.out.println("");
        while(number3<slash){
          System.out.print("/ "); // same functions as number 1, outputs front slash as bottom of twist
          number3=number3+2; 
          if(number3<slash);{
            System.out.print("\\");
            number3++; // increments if less than length
          }
        }
      }
else{
        System.out.println("Please enter a positive integer"); // if the user enters an integer less than 0 the terminal prompts them to enter another positive integer
      }
    }
else{
        System.out.println("Enter an integer"); // if the user does not enter an integer, such as a double or float, they're prompted to enter one
      
      }
   
       }
    } // end of main method
  } // end of class