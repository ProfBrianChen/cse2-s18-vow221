//Vasilios Oliver Walsh
import java.util.Scanner;
public class Yahtzee {
  public static void main(String[] args) {
  //main method required for every Java program
  Scanner input = new Scanner (System.in);
    System.out.print ("Would you like to roll the dice? Enter yes, or no to manually enter a 5 digit number: "); // prompts the user to input whether they want a random dice roll
       String enter = input.nextLine();
       String yes = "yes";
       String no = "no";
       int dice1, dice2, dice3, dice4, dice5 = 0; // declaring variables for later in the code
    dice1 = (int)(Math.random()*6)+1;
    dice2 = (int)(Math.random()*6)+1;
    dice3 = (int)(Math.random()*6)+1;
    dice4 = (int)(Math.random()*6)+1;
    dice5 = (int)(Math.random()*6)+1; // sets the dice roll to be random if user prompts yes
   if (enter.equals("yes")) {
     System.out.println(dice1 + "" + dice2 + "" + dice3 + "" + dice4 + "" + dice5);
   } // if user enters yes, random dice variables add together
    else if (enter.equals("no")) {
     System.out.print("Enter your first roll: ");
    dice1 = input.nextInt();
      System.out.print("Enter your second roll: ");
    dice2 = input.nextInt();
       System.out.print("Enter your third roll: ");
    dice3 = input.nextInt();
         System.out.print("Enter your fourth roll: ");
    dice4 = input.nextInt();
           System.out.print("Enter your fifth roll: ");
    dice5 = input.nextInt();
      } // if user enters no, prompts them to enter their desired dice rolls
    boolean error = false;
    if (dice1 == 0 || dice1 > 6) {
      error = true;
    }
    else if (dice2 == 0 || dice2 > 6) {
      error = true;
    }
    else if (dice3 == 0 || dice3 > 6) {
      error = true;
    }
    else if (dice4 == 0 || dice4 > 6) {
      error = true;
    }
    else if (dice5 == 0 || dice5 > 6) {
      error = true;
    }
    if (error == false) {
        System.out.println("no error"); // if error is false, prints no error
    }
    else if (error == true) {
        System.out.println("impossible dice value"); // if error is false, prints impossible 
    }
    int count1 = 0;
    int count2 = 0;
    int count3 = 0;
    int count4 = 0;
    int count5 = 0;
    int count6 = 0; // sets count variables to 0 
    if (dice1 == 1) {
      count1 = count1 + 1;
    } else if (dice1 == 2) {
      count2 = count2 + 1;
    } else if (dice1 == 3) {
      count3 = count3 + 1;
    } else if (dice1 == 4) {
      count4 = count4 + 1;
    } else if (dice1 == 5) {
      count5 = count5 + 1;
    } else if (dice1 == 6) {
      count6 = count6 + 1; // if dice is rolled, count variable adds 1 to indicate a roll
    }
     if (dice2 == 1) {
      count1 = count1 + 1;
    } else if (dice2 == 2) {
      count2 = count2 + 1;
    } else if (dice2 == 3) {
      count3 = count3 + 1;
    } else if (dice2 == 4) {
      count4 = count4 + 1;
    } else if (dice2 == 5) {
      count5 = count5 + 1;
    } else if (dice2 == 6) {
      count6 = count6 + 1;
    }
     if (dice3 == 1) {
      count1 = count1 + 1;
    } else if (dice3 == 2) {
      count2 = count2 + 1;
    } else if (dice3 == 3) {
      count3 = count3 + 1;
    } else if (dice3 == 4) {
      count4 = count4 + 1;
    } else if (dice3 == 5) {
      count5 = count5 + 1;
    } else if (dice3 == 6) {
      count6 = count6 + 1;
    }
     if (dice4 == 1) {
      count1 = count1 + 1;
    } else if (dice4 == 2) {
      count2 = count2 + 1;
    } else if (dice4 == 3) {
      count3 = count3 + 1;
    } else if (dice4 == 4) {
      count4 = count4 + 1;
    } else if (dice4 == 5) {
      count5 = count5 + 1;
    } else if (dice4 == 6) {
      count6 = count6 + 1;
    }
     if (dice5 == 1) {
      count1 = count1 + 1;
    } else if (dice5 == 2) {
      count2 = count2 + 1;
    } else if (dice5 == 3) {
      count3 = count3 + 1;
    } else if (dice5 == 4) {
      count4 = count4 + 1;
    } else if (dice5 == 5) {
      count5 = count5 + 1;
    } else if (dice5 == 6) {
      count6 = count6 + 1;
    }
    int Score = 0;
    if (count1 == 5 || count2 == 5 || count3 == 5 || count4 == 5 || count5 == 5) {
    System.out.println ("Yahtzee: Score 50"); // yahtzee initializer 
    }
    else if (count1 == 1 && count2 == 1 && count3 == 1 && count4 == 1 && count5 == 1 || count2 == 1 && count3 == 1 && count4 == 1 && count5 == 1 && count6 == 1) {
    System.out.println ("Large Straight: Score 40"); // large straight indicator
    }
    else if (count1 == 1 && count2 == 1 && count3 == 1 && count4 == 1 || count2 == 1 && count3 == 1 && count4 == 1 && count5 == 1 || count3 == 1 && count4 == 1 && count5 == 1 && count6 == 1) {
    System.out.println ("Small Straight: Score 30"); // small straight indicator
    }
    else if (count1 == 3 && (count2 == 2 || count3 == 2 || count4 == 2 || count5 == 2 || count6 == 2) ||
            count2 == 3 && (count1 == 2 || count3 == 2 || count4 == 2 || count5 == 2 || count6 == 2) ||
            count3 == 3 && (count2 == 2 || count1 == 2 || count4 == 2 || count5 == 2 || count6 == 2) ||
            count4 == 3 && (count2 == 2 || count3 == 2 || count1 == 2 || count5 == 2 || count6 == 2) ||
            count5 == 3 && (count2 == 2 || count3 == 2 || count4 == 2 || count1 == 2 || count6 == 2) ||
            count6 == 3 && (count2 == 2 || count3 == 2 || count4 == 2 || count5 == 2 || count1 == 2)) {
      System.out.println ("Full House: Score 25"); // full house indicator
    }
    else if (count1 == 4 || count2 == 4 || count3 == 4 || count4 == 4 || count5 == 4 || count6 == 4) {
      Score = (dice1 + dice2 + dice3 + dice4 + dice5);
      System.out.println ("4 of a kind: Score " + Score); // 4 of a kind initializer, adds score at end
    }
    else if (count1 == 3 || count2 == 3 || count3 == 3 || count4 == 3 || count5 == 3 || count6 == 3) {
            Score = (dice1 + dice2 + dice3 + dice4 + dice5);
      System.out.println ("3 of a kind: Score " + Score); // 3 of a kind initializer, adds score at end
    }
    else {
      Score = (dice1 + dice2 + dice3 + dice4 + dice5);
      System.out.println ("Chance: Score " + Score); // chance initializer
    }
              
  }
}