//Vasilios Oliver Walsh
import java.util.Arrays;
import java.util.Random;
public class RobotCity {
  
  public static int[][] buildCity(){
    int cityArray[][] = new int[(int)(Math.random()*6)+10][(int)(Math.random()*6)+10]; // sets multidimensional array values equal to random integers between 10 and 15
    for(int i = 0; i < cityArray.length; i++){ // count variables
      for(int j = 0; j < cityArray[0].length; j++){
        cityArray[i][j] = (int)(Math.random()*(900))+100; // sets members of index i and j between 100 and 999
    }
    }
    return cityArray;
  }
  
  public static void display(int[][] population){ 
    for(int i = 0; i < population.length; i++){ 
      for(int j = 0; j < population[i].length; j++){
        System.out.printf("%5d", population[i][j]); // function of print f to display population variable in grid
       }
         System.out.println();
  }
}
  
  public static void invade(int[][] city, int robots){
     for(int i = 0; i < robots; i++){
        int x = (int)(Math.random()*(city.length)); // randomly decides which integers should be multiplied by negative one to indicate robot presence
        int y = (int)(Math.random()*(city[0].length)); 
          if(city[x][y] >= 0){
            city[x][y] *= -1;
   }
  }
 }
  
  public static void update(int[][] city){ 
    for(int i = 0; i < city.length; i++){
     int[] row = city[i];
     if(row[row.length - 1] <= 0) { // updates the integers which have been changed to negative integers
      row[row.length - 1] *= -1;
    }
     for(int j = row.length - 1; j >= 0; j--){ 
       if(row[j] < 0){
          row[j+1] *= -1;
          row[j] *=-1;
      }
    }
  }
  }
  
  public static void main(String[] args){
    int cityArray[][] = buildCity();
    int[][] population = buildCity(); 
   for(int i = 0; i < 5; i++){ // outputs all other methods 
     display(population); 
      System.out.println();
    int robots = (int)(Math.random() * 5); // sets robots variable so it may be used as input for other functions
    invade(population, robots);
    display(population);
      System.out.println();
    update(population);
    display(population);
      System.out.println();
    }  
  }
}
