// This program will run a random number generator in order to output different cards from a deck.
public class CardGenerator {
  public static void main(String[] args) {
    int card = (int)(Math.random()*51)+1;
    String chosenCard = null;
    String type;
  if (card <= 13) {
   chosenCard = "Spade";
  }if (14 <= card && card <= 26) {
    chosenCard = "Diamond";
    //type = card - 1;
  }if (27 <= card && card <= 39) {
    chosenCard = "Heart";
    //type = card - 1;
  }if (40 <= card && card <=52){
    chosenCard = "Clubs";
   // type = card - 1;
  }    
    switch (card % 13){
      case 1:
        System.out.println("Ace of " + chosenCard);
        break;
      case 2:
        System.out.println("2 of " + chosenCard);
        break;
      case 3:
        System.out.println("3 of " + chosenCard);
        break;
      case 4:
        System.out.println("4 of " + chosenCard);
        break;
      case 5:
        System.out.println("5 of " + chosenCard);
        break;
      case 6:
        System.out.println("6 of " + chosenCard);
        break;
      case 7:
        System.out.println("7 of " + chosenCard);
        break;
      case 8:
        System.out.println("8 of " + chosenCard);
        break;
      case 9:
        System.out.println("9 of " + chosenCard);
        break;
      case 10:
        System.out.println("10 of " + chosenCard);
        break;
      case 11:
        System.out.println("Jack of " + chosenCard);
        break;
      case 12:
        System.out.println("Queen of " + chosenCard);
        break;
      case 13:
        System.out.println("King of " + chosenCard);
        break;
    }                   
  } // end of main method
} // end of class