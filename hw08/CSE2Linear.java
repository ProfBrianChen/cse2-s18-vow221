//Vasilios Oliver Walsh

import java.util.Scanner;
import java.util.Arrays;
public class CSE2Linear {

public static void main (String[]args){ // main method
  Scanner scan = new Scanner(System.in); // imports a new instance of a scanner
  
  int i = 0; // declares integer i, which is to be used as a count 
  int[] grades = new int[15]; // array declaration and length assignemnt 
  System.out.println("Please enter 15 integers in ascending order to define the final scores of CSE2 students: "); // prompts the user to input 15 grades
  
  while (i < grades.length - 1){ // while the count variable is less than the length of the grades array
    if (scan.hasNextInt()){ // automatic conditional
        grades[i] = scan.nextInt();  // assignes grade at count i
     if (grades[i] < 0 || grades[i] > 100){ // states that if array is less than 0 or over 100 then enter an int between the parameters
        System.out.println("Please enter an integer between 0 and 100: "); 
   }
    else if (i > 0){// moving onto conditional in case the inputted grade is less than the previous
       if (grades[i] < grades[i - 1]){ 
        System.out.println("Please enter the integers in ascending order: "); // makes sure user has entered in ascending order by taking grades' last array value and checking if previous is larger
                                     } // end of if statement
                   } // end of else if statement
       i++; // increment i, moves to next index in the array after user inputs previous grade
        }
    } // end of while loop
      
    if (scan.hasNextInt() == false){ // in case user doesn't enter an integer
      System.out.println("Please enter an int:");
      scan.nextLine();
    } // end of if statement
      
  int searchedgrade = 0; // second variable declaration, to be used for the grade in which the user wishes to search for in the array "grades"
        if (searchedgrade<1){
          System.out.println("Please enter which grade you would like to search for: "); // prompts user for grade they want to search for
          
          searchedgrade = scan.nextInt(); // initalized variable is replaced
          
          System.out.println("Binary: "); // calls binary search method
             System.out.println(BinarySearch(grades, searchedgrade)); // prints binary search 
          System.out.println("Linear: "); 
            System.out.println(LinearSearch(grades, searchedgrade)); // prints the sorted
          System.out.println("Randomly Scrambled:");  // calls random scrambling method
            System.out.println(RandomScrambling(grades));
        }
  } // end of main method
    
    public static int LinearSearch(int[] grades, int searchedgrade){ // linear search method
        for (int i = 0; i < grades.length; i++){ // increments i, counter variable, when it is less than the length, sets conditional
            if (grades[i] == searchedgrade){ // if the entered grade is equal to any of the array indexes, it will print the next line
                System.out.println(searchedgrade + " was found in the list in " + (i + 1) + " iterations"); // was found output
                break;
            }
            else if (grades[i] != searchedgrade && i == (grades.length - 1)){ // 
                System.out.println(searchedgrade + " was not found in " + (i - 1) + " iterations"); // wasnt found output
             
            }
        }
       return searchedgrade;
    }
  
    public static int BinarySearch(int[] grades, int binarygrade){ // binary search method
        int top = grades.length - 1;
        int bottom = 0;
        int i = 0;
        while (bottom <= top){
            i++;
            int middle = (top + bottom)/2;
         if (binarygrade < grades[middle]){
            top = middle - 1;
            }
          else if (binarygrade > grades[middle]){
             bottom = middle + 1;   
            }
            else if (binarygrade == grades[middle]){
              System.out.println(binarygrade + " was found in " + i + " iterations");
              break;
            }
        }
        if (bottom > top){
           System.out.println(binarygrade + " was not found in " + i + " iterations");
        }
      return binarygrade;
    }

    public static int[] RandomScrambling(int[] grades){ // random scrambling method
        for (int i = 0; i < grades.length; i++){
          int score = grades[i];  
          int x = (int)(Math.random()*grades.length);
        while (x != i){
          grades[x] = score;      
          grades[i] = grades[x];
          break;
            }
        }
        return grades;
    }
}