//Vasilios Oliver Walsh
import java.util.Scanner;
public class Area { 
  public static String Shape() {
  Scanner myScanner = new Scanner (System.in); 
  System.out.println("Please enter the desired shape, being a rectangle, triangle, or a circle: ");
  String shape = myScanner.next();
  boolean isShape = true;
    
  while (isShape) {
    if (shape.equals ("rectangle")){
      break; }
    else if (shape.equals ("triangle")){
      break; }
    else if (shape.equals("circle")){
      break; }
    else { 
      System.out.println("Please enter one of 'rectangle', triangle', or 'circle'"); 
    shape = myScanner.next();}
  }
    return shape;
  } // ends first method
public static double rectangleInput (){
  Scanner myScanner = new Scanner (System.in); 
  System.out.println("Please enter a double value to define the length of the rectangle: ");
  while (myScanner.hasNextDouble() == false) {
    System.out.println("Error, please enter a double");
    String junkword = myScanner.nextLine();
  }
    double length = myScanner.nextDouble();
  System.out.println("Please enter a double value to define the width of the rectangle: ");
  while (myScanner.hasNextDouble() == false) {
    System.out.println("Error, please enter a double");
    String junkword = myScanner.nextLine();
  }
  double width = myScanner.nextDouble();
  double area = length * width;
  return area;
}
  public static double triangleInput (){
  Scanner myScanner = new Scanner (System.in); 
  System.out.println("Please enter a double value to define the base of the triangle: ");
  while (myScanner.hasNextDouble() == false) {
    System.out.println("Error, please enter a double");
    String junkword = myScanner.nextLine();
  }
    double base = myScanner.nextDouble();
  System.out.println("Please enter a double value to define the height of the triangle: ");
  while (myScanner.hasNextDouble() == false) {
    System.out.println("Error, please enter a double");
    String junkword = myScanner.nextLine();
  }
  double height = myScanner.nextDouble();
  double area = ((1/2) * base * height);
  return area;
  }
  public static double circleInput (){
  Scanner myScanner = new Scanner (System.in); 
  double Pi = 3.14159265359;
  System.out.println("Please enter a double value to define the radius of the triangle: ");
  while (myScanner.hasNextDouble() == false) {
    System.out.println("Error, please enter a double");
    String junkword = myScanner.nextLine();
  }
  double radius = myScanner.nextDouble();
  double area = (Pi * radius * radius);
  return area;
}
  public static void main(String[] args){
    String shape = Shape();
    double area = 0;
      if(shape.equals("rectangle")){
        area = rectangleInput();
      } else if (shape.equals("triangle")){
        area = triangleInput();
      } else if (shape.equals("circle")){
        area = circleInput();
      }
    System.out.println("Area of the " + shape + " is: " + area);
  }
}
  